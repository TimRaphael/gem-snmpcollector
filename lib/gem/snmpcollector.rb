require 'gem/snmpcollector/version'
require 'rest-client'
require 'json'
require 'json-diff'

module Snmpcollector

  class API

    def initialize(url, username, password)
      @url = url
      @cookie_jar = authenticate(username, password)
    end

    def authenticate(username, password)
      auth_resp = RestClient.post "#{@url}/login",
                                  {username: username, password: password},
                                  {content_type: 'json', accept: 'json'}

      @cookie_jar = auth_resp.cookie_jar
    end

    def authenticated?
      @cookie_jar != nil
    end

    def get_device_config(device)
      if authenticated?
        resp = RestClient.get "#{@url}/api/cfg/snmpdevice/#{device}",
                              cookies: @cookie_jar
        JSON.parse resp.body
      end
    end

    def config_match?(config_one, config_two)
      diff = JsonDiff.diff config_one, config_two

      diff.empty?
    end

  end

end